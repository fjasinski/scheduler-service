# Simple Weather App

The purpose of this project is to create an interactive weather application 
which helps in diagnosis of environmental and climate changes through collection of 
essential data and appropriate visualisation of gathered statistics. The data will 
get collected from OpenWeatherMap and AirQualityIndex APIs.

```
https://openweathermap.org/api
https://aqicn.org/api/pl/
```

*Please bare in mind, this is just one of the few microservices.*

## Scheduler Service

This repository shares one of the essential services which is responsible for 
collection of data from the APIs mentioned above. Gatehered data gets validated 
and posted using REST to selected services which are responsible for data storage. 
The communication with other application's services is handled through Eureka 
service registry. 

There are still some other things to consider implementing. One of them is
CicruitBreaker pattern to detect failutes and prevent them in further requests.


### Project Architecture

For enhanced flexibility and ease of implementation I have decided to develop
this project in microservice manner. Independent, individual elements allow
simplicity in terms of code management and maintenance.

Each microservice is built with Spring Boot for ease of setup and WebFlux along
with MongoDb for non-blocking, asynchronous operations. As the basis I have 
decided to take Domain Driven Design approach.

