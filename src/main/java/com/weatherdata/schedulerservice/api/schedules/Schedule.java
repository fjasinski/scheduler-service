package com.weatherdata.schedulerservice.api.schedules;

import org.springframework.scheduling.annotation.Scheduled;

/**
 * Cron scheduler.
 */
public interface Schedule {

    /**
     * Run the following tasks in a schedule
     */
    void schedule();
}
