package com.weatherdata.schedulerservice.api.schedules.impl;

import com.weatherdata.schedulerservice.api.schedules.Schedule;
import com.weatherdata.schedulerservice.application.DataFetchingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class WeatherForecastSchedule implements Schedule {

    @Autowired
    private DataFetchingService forecastWeatherFetchingService;

    @Override
    @Scheduled(cron = "0 0 * * * *")
    public void schedule() {
        forecastWeatherFetchingService.fetch();
    }
}
