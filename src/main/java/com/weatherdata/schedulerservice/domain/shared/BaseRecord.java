package com.weatherdata.schedulerservice.domain.shared;

import lombok.*;
import org.joda.time.DateTime;

@Getter
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Builder(toBuilder = true)
@ToString
public class BaseRecord {

    private String id;

    private int weatherId;

    private String cityId;

    private String weatherTitle;

    private String weatherDescription;

    private String weatherIcon;

    private double currentTemperature;

    private double minTemperature;

    private double maxTemperature;

    private double windSpeed;

    private int windDegrees;

    private int clouds;

    private int humidity;

    private double seaLevel;

    private double groundLevel;

    private int pressure;

    private int visibility;

    private DateTime forecastTime;

    private DateTime readingTimestamp;

    private DateTime createdAt;
}
