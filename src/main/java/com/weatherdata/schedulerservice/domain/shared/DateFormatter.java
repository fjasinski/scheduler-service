package com.weatherdata.schedulerservice.domain.shared;

import org.joda.time.DateTime;

/**
 * Date formatter.
 */
public interface DateFormatter {

    /**
     * @param stringDate String e.g. 2019-01-01 11:11:11
     * @param format String e.g. yyyy-MM-dd HH:mm:ss
     * @return DateTime
     */
    DateTime dateFromString(String stringDate, String format);


    /**
     * @param unixTimestamp
     * @return DateTime
     */
    DateTime dateFromUnixTimestamp(Long unixTimestamp);

    /**
     * @return DateTime
     */
    DateTime newDate();
}
