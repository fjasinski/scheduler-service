package com.weatherdata.schedulerservice.domain.shared.impl;

import com.weatherdata.schedulerservice.domain.shared.DateFormatter;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

@Component
public class DateFormatterImpl implements DateFormatter {

    @Override
    public DateTime dateFromString(String stringDate, String format) {
        DateTimeFormatter dtf = DateTimeFormat.forPattern(format);
        return dtf.parseDateTime(stringDate);
    }

    @Override
    public DateTime dateFromUnixTimestamp(Long unixTimestamp) {
        return new DateTime(unixTimestamp * 1000L).withZone(DateTimeZone.UTC);
    }

    public DateTime newDate() {
        return new DateTime().withZone(DateTimeZone.UTC);
    }
}
