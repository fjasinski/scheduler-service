package com.weatherdata.schedulerservice.domain.shared;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.weatherdata.schedulerservice.domain.city.CityDto;
import reactor.core.publisher.Mono;

/**
 * Record storing service
 */
public interface RecordStoringService<T> {

    /**
     * Store object to repository
     *
     * @param objectNode object
     * @param cityDto object
     * @return
     */
    Mono<T> storeRecord(Mono<ObjectNode> objectNode, CityDto cityDto);
}
