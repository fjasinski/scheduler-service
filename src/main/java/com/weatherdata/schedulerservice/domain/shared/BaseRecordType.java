package com.weatherdata.schedulerservice.domain.shared;

public enum BaseRecordType {
    PRESENT, FORECAST
}
