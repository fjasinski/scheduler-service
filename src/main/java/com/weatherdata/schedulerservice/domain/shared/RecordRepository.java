package com.weatherdata.schedulerservice.domain.shared;

import com.weatherdata.schedulerservice.domain.city.CityDto;
import com.weatherdata.schedulerservice.domain.shared.BaseRecord;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface RecordRepository<T> {


    /**
     * POST storing request to archive service repository.
     *
     * @param baseRecord BaseRecord object
     * @return Mono BaseRecord
     */
    Mono<T> postRecord(T baseRecord);


    /**
     * GET request to archive service repository.
     * Retrieves latest weather record for given city.
     *
     * @param cityDto CityDto object
     * @return Mono BaseRecord
     */
    Mono<T> getLatestRecordByCityId(CityDto cityDto);
}
