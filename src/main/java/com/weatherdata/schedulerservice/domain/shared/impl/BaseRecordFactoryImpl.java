package com.weatherdata.schedulerservice.domain.shared.impl;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.weatherdata.schedulerservice.domain.city.CityDto;
import com.weatherdata.schedulerservice.domain.shared.BaseRecord;
import com.weatherdata.schedulerservice.domain.shared.BaseRecordFactory;
import com.weatherdata.schedulerservice.domain.shared.BaseRecordType;
import com.weatherdata.schedulerservice.domain.shared.DateFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class BaseRecordFactoryImpl implements BaseRecordFactory {

    @Autowired
    private DateFormatter dateFormatter;

    /**
     * Form and return a BaseWeatherRecord object with primary parameters
     * for further processing.
     *
     * @param objectNode object
     * @param cityDto object
     * @return Mono BaseWeatherRecord object
     */
    @Override
    public Mono<BaseRecord> getObject(ObjectNode objectNode, CityDto cityDto, BaseRecordType baseRecordType) {

        final BaseRecord.BaseRecordBuilder builder = BaseRecord.builder();

        if (objectNode.has("weather") && objectNode.get("weather").has(0)) {

            if (objectNode.get("weather").get(0).has("id")) {
                builder.weatherId(objectNode.get("weather").get(0).get("id").asInt());
            }

            if (objectNode.get("weather").get(0).has("main")) {
                builder.weatherTitle(objectNode.get("weather").get(0).get("main").toString());
            }

            if (objectNode.get("weather").get(0).has("description")) {
                builder.weatherDescription(objectNode.get("weather").get(0).get("description").toString());
            }

            if (objectNode.get("weather").get(0).has("icon")) {
                builder.weatherIcon(objectNode.get("weather").get(0).get("icon").toString());
            }
        }

        if (objectNode.has("main")) {

            if (objectNode.get("main").has("temp")) {
                builder.currentTemperature(objectNode.get("main").get("temp").asDouble());
            }

            if (objectNode.get("main").has("temp_min")) {
                builder.minTemperature(objectNode.get("main").get("temp_min").asDouble());
            }

            if (objectNode.get("main").has("temp_max")) {
                builder.maxTemperature(objectNode.get("main").get("temp_max").asDouble());
            }

            if (objectNode.get("main").has("humidity")) {
                builder.humidity(objectNode.get("main").get("humidity").asInt());
            }

            if (objectNode.get("main").has("sea_level")) {
                builder.seaLevel(objectNode.get("main").get("sea_level").asDouble());
            }

            if (objectNode.get("main").has("grnd_level")) {
                builder.groundLevel(objectNode.get("main").get("grnd_level").asDouble());
            }

            if (objectNode.get("main").has("pressure")) {
                builder.pressure(objectNode.get("main").get("pressure").asInt());
            }
        }

        if (objectNode.has("wind")) {

            if (objectNode.get("wind").has("speed")) {
                builder.windSpeed(objectNode.get("wind").get("speed").asDouble());
            }

            if (objectNode.get("wind").has("deg")) {
                builder.windDegrees(objectNode.get("wind").get("deg").asInt());
            }
        }

        if (objectNode.has("clouds") && objectNode.get("clouds").has("all")) {
            builder.clouds(objectNode.get("clouds").get("all").asInt());
        }

        if (objectNode.has("visibility")) {
            builder.visibility(objectNode.get("visibility").asInt());
        }


        if (baseRecordType.equals(BaseRecordType.FORECAST)) {
            builder.forecastTime(dateFormatter.dateFromUnixTimestamp(objectNode.get("dt").asLong()));
        } else {
            builder.readingTimestamp(dateFormatter.dateFromUnixTimestamp(objectNode.get("dt").asLong()));
            builder.cityId(cityDto.getId());
        }

        builder.createdAt(dateFormatter.newDate());

        return Mono.fromCallable(builder::build);
    }
}
