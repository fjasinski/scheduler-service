package com.weatherdata.schedulerservice.domain.shared;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.weatherdata.schedulerservice.domain.city.CityDto;
import reactor.core.publisher.Mono;

/**
 * Weather object factory.
 */
public interface BaseRecordFactory {


    /**
     * Builds "Type" object for given city based on provided JSON.
     *
     * @param objectNode object
     * @param cityDto object
     * @return Mono T object
     */
    Mono<BaseRecord> getObject(ObjectNode objectNode, CityDto cityDto, BaseRecordType baseRecordType);
}
