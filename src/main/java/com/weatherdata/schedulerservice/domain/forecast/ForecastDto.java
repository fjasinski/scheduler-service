package com.weatherdata.schedulerservice.domain.forecast;

import com.weatherdata.schedulerservice.domain.shared.BaseRecord;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.joda.time.DateTime;

import java.util.List;

@Getter
@Setter
@ToString
public class ForecastDto {

    private String id;

    private String cityId;

    private List<BaseRecord> recordList;

    private DateTime createdAt;
}
