package com.weatherdata.schedulerservice.domain.forecast;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.weatherdata.schedulerservice.domain.city.CityDto;
import com.weatherdata.schedulerservice.domain.shared.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.StreamSupport;

@Component
public class ForecastService implements RecordStoringService<ForecastDto> {

    @Autowired
    private BaseRecordFactory baseRecordFactory;

    @Autowired
    private RecordRepository<ForecastDto> forecastServiceRepository;

    @Autowired
    private DateFormatter dateFormatter;

    @Override
    public Mono<ForecastDto> storeRecord(Mono<ObjectNode> objectNode, CityDto cityDto) {
        return objectNode.flatMap(data -> {
            Mono<ForecastDto> forecastRecord = prepareData(data, cityDto);
            return isRecordValid(forecastRecord, cityDto).flatMap(result -> {
                if (!result) {
                    return forecastRecord;
                }
                return forecastRecord.flatMap(latest -> forecastServiceRepository.postRecord(latest));
            });
        });
    }

    /**
     * Prepares a new forecast object
     *
     * @param objectNode ObjectNode
     * @param cityDto CityDto
     * @return Mono ForecastDto
     */
    private Mono<ForecastDto> prepareData(ObjectNode objectNode, CityDto cityDto) {
        final ForecastDto forecastDto = new ForecastDto();
        final List<BaseRecord> baseRecordList = new ArrayList<>();

        if(!objectNode.has("list")) {
            return Mono.fromCallable(ForecastDto::new);
        }

        final JsonNode nodeList = objectNode.get("list");
        StreamSupport.stream(nodeList.spliterator(), false)
                .forEach(forecast -> baseRecordFactory
                        .getObject(forecast.deepCopy(), cityDto, BaseRecordType.FORECAST)
                        .subscribe(baseRecordList::add));

        forecastDto.setRecordList(baseRecordList);
        forecastDto.setCreatedAt(dateFormatter.newDate());
        forecastDto.setCityId(cityDto.getId());

        return Mono.fromCallable(() -> forecastDto);
    }

    /**
     * Validates new forecast object
     *
     * @param forecastDto
     * @param cityDto
     * @return
     */
    private Mono<Boolean> isRecordValid(Mono<ForecastDto> forecastDto, CityDto cityDto) {

        return forecastDto.flatMap(latest -> {
            if (latest.getCreatedAt() == null) {
                return Mono.fromCallable(() -> false);
            }

            final Mono<ForecastDto> lastSavedRecord = forecastServiceRepository
                    .getLatestRecordByCityId(cityDto);
            return lastSavedRecord.flatMap(lastSaved -> {
               if (lastSaved.getId() == null) {
                   return Mono.fromCallable(() -> true);
               }

               final BaseRecord latestBaseRecord = latest.getRecordList().get(0);
               final BaseRecord lastSavedBaseRecord = lastSaved.getRecordList().get(0);
               return Mono.fromCallable(() -> latestBaseRecord.getForecastTime().compareTo(lastSavedBaseRecord.getForecastTime()) > 0);
            });

        });
    }
}
