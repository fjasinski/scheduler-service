package com.weatherdata.schedulerservice.domain.city;

import lombok.Data;

@Data
public class CityDto {

    private String id;

    private String name;

    private String countryCode;
}
