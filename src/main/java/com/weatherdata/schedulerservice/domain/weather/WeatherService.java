package com.weatherdata.schedulerservice.domain.weather;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.weatherdata.schedulerservice.domain.city.CityDto;
import com.weatherdata.schedulerservice.domain.shared.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class WeatherService implements RecordStoringService<BaseRecord> {

    @Autowired
    private RecordRepository<BaseRecord> archiveServiceRepository;

    @Autowired
    private BaseRecordFactory baseRecordFactory;

    @Override
    public Mono<BaseRecord> storeRecord(Mono<ObjectNode> objectNode, CityDto cityDto) {
        return objectNode.flatMap(data -> {
            final Mono<BaseRecord> baseRecord = baseRecordFactory.getObject(data, cityDto, BaseRecordType.PRESENT);
            return isRecordValid(baseRecord, cityDto).flatMap(result -> {
               if (!result) {
                   return baseRecord;
               }
               return baseRecord.flatMap(latest -> archiveServiceRepository.postRecord(latest));
            });
        });
    }

    /**
     * Validates retrieved record by comparing the timestamp.
     * Returns true if there is no last saved record.
     *
     * @param baseRecord Mono BaseRecord object
     * @param cityDto CityDto object
     * @return Mono true
     */
    private Mono<Boolean> isRecordValid(Mono<BaseRecord> baseRecord, CityDto cityDto) {
        final Mono<BaseRecord> lastSavedRecord = archiveServiceRepository
                .getLatestRecordByCityId(cityDto);

        return lastSavedRecord.flatMap(lastSaved -> {
            if (lastSaved.getId() == null) {
                return Mono.fromCallable(() -> true);
            }
            return baseRecord.map(latest -> latest.getReadingTimestamp().compareTo(lastSaved.getReadingTimestamp()) > 0);
        });
    }

}
