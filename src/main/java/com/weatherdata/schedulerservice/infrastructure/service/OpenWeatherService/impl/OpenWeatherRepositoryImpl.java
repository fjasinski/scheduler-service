package com.weatherdata.schedulerservice.infrastructure.service.OpenWeatherService.impl;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.weatherdata.schedulerservice.domain.city.CityDto;
import com.weatherdata.schedulerservice.infrastructure.service.OpenWeatherService.OpenWeatherRepository;
import com.weatherdata.schedulerservice.infrastructure.service.OpenWeatherService.OpenWeatherUrlBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class OpenWeatherRepositoryImpl implements OpenWeatherRepository {

    @Autowired
    private WebClient.Builder webClientBuilder;

    @Autowired
    private OpenWeatherUrlBuilder openWeatherUrlBuilder;

    @Override
    public Mono<ObjectNode> getWeatherPresent(CityDto cityDto) {
        return openWeatherUrlBuilder
                .presentWeatherUrl(cityDto)
                .flatMap(this::makeRequest);
    }

    @Override
    public Mono<ObjectNode> getWeatherForecast(CityDto cityDto) {
        return openWeatherUrlBuilder
                .forecastWeatherUrl(cityDto)
                .flatMap(this::makeRequest);
    }

    /**
     * Base method for making calls to Open Weather API
     * @param url String
     * @return Mono ObjectNode object
     */
    private Mono<ObjectNode> makeRequest(String url) {
        return webClientBuilder
                .build()
                .get()
                .uri(url)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(ObjectNode.class);
    }
}
