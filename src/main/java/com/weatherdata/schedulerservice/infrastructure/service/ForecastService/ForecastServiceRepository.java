package com.weatherdata.schedulerservice.infrastructure.service.ForecastService;

import com.weatherdata.schedulerservice.domain.city.CityDto;
import com.weatherdata.schedulerservice.domain.forecast.ForecastDto;
import com.weatherdata.schedulerservice.domain.shared.RecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class ForecastServiceRepository implements RecordRepository<ForecastDto> {

    @Autowired
    private WebClient.Builder loadBalancedWebClientBuilder;

    @Override
    public Mono<ForecastDto> postRecord(ForecastDto forecastDto) {
        return loadBalancedWebClientBuilder
                .build()
                .post()
                .uri("http://forecast-service/forecast")
                .contentType(MediaType.APPLICATION_STREAM_JSON)
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .body(BodyInserters.fromObject(forecastDto))
                .retrieve()
                .bodyToMono(ForecastDto.class);
    }

    @Override
    public Mono<ForecastDto> getLatestRecordByCityId(CityDto cityDto) {
        return loadBalancedWebClientBuilder
                .build()
                .get()
                .uri("http://forecast-service/forecast/" + cityDto.getId() + "/latest")
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .retrieve()
                .bodyToMono(ForecastDto.class)
                .onErrorReturn(new ForecastDto());
    }
}
