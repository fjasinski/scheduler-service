package com.weatherdata.schedulerservice.infrastructure.service.OpenWeatherService;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.weatherdata.schedulerservice.domain.city.CityDto;
import reactor.core.publisher.Mono;

/**
 * OpenWeather API repository.
 */
public interface OpenWeatherRepository {


    /**
     * Retrieve present weather reading for given city.
     *
     * @param cityDto object
     * @return Mono ObjectNode object
     */
    Mono<ObjectNode> getWeatherPresent(CityDto cityDto);


    /**
     * Retrieve weather forecast reading for given city.
     *
     * @param cityDto object
     * @return Mono ObjectNode object
     */
    Mono<ObjectNode> getWeatherForecast(CityDto cityDto);
}
