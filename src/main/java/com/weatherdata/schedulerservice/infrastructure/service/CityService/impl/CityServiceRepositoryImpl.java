package com.weatherdata.schedulerservice.infrastructure.service.CityService.impl;

import com.weatherdata.schedulerservice.domain.city.CityDto;
import com.weatherdata.schedulerservice.infrastructure.service.CityService.CityServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@Component
public class CityServiceRepositoryImpl implements CityServiceRepository {

    @Autowired
    private WebClient.Builder loadBalancedWebClientBuilder;

    @Override
    public Flux<CityDto> getCities() {
        return loadBalancedWebClientBuilder
                .build()
                .get()
                .uri("http://city-service/cities/list")
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .retrieve()
                .bodyToFlux(CityDto.class);
    }
}
