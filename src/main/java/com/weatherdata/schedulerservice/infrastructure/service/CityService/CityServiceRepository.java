package com.weatherdata.schedulerservice.infrastructure.service.CityService;

import com.weatherdata.schedulerservice.domain.city.CityDto;
import reactor.core.publisher.Flux;

/**
 * Weather App's city service repository.
 */
public interface CityServiceRepository {

    /**
     * Retrieves city json stream from the app's city service
     *
     * @return Flux CityDto object
     */
    Flux<CityDto> getCities();
}
