package com.weatherdata.schedulerservice.infrastructure.service.OpenWeatherService;

import com.weatherdata.schedulerservice.domain.city.CityDto;
import reactor.core.publisher.Mono;

/**
 * OpenWeather API's URL builder.
 */
public interface OpenWeatherUrlBuilder {

    /**
     * Returns present weather repository URL for the given city.
     *
     * @param cityDto object
     * @return Mono string object
     */
    Mono<String> presentWeatherUrl(CityDto cityDto);


    /**
     * Returns forecast weather repository URL for the given city.
     *
     * @param cityDto object
     * @return Mono string object
     */
    Mono<String> forecastWeatherUrl(CityDto cityDto);
}
