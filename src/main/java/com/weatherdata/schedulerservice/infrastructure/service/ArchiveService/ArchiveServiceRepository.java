package com.weatherdata.schedulerservice.infrastructure.service.ArchiveService;

import com.weatherdata.schedulerservice.domain.city.CityDto;
import com.weatherdata.schedulerservice.domain.shared.BaseRecord;
import com.weatherdata.schedulerservice.domain.shared.RecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * Weather app's archive service repository
 */
@Component
public class ArchiveServiceRepository implements RecordRepository<BaseRecord> {

    @Autowired
    private WebClient.Builder loadBalancedWebClientBuilder;

    @Override
    public Mono<BaseRecord> postRecord(BaseRecord baseRecord) {
        return loadBalancedWebClientBuilder
                .build()
                .post()
                .uri("http://archive-service/archive")
                .contentType(MediaType.APPLICATION_STREAM_JSON)
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .body(BodyInserters.fromObject(baseRecord))
                .retrieve()
                .bodyToMono(BaseRecord.class);
    }

    @Override
    public Mono<BaseRecord> getLatestRecordByCityId(CityDto cityDto) {
            return loadBalancedWebClientBuilder
                    .build()
                    .get()
                    .uri("http://archive-service/archive/" + cityDto.getId() + "/latest")
                    .accept(MediaType.APPLICATION_STREAM_JSON)
                    .retrieve()
                    .bodyToMono(BaseRecord.class)
                    .onErrorReturn(BaseRecord.builder().build());
    }
}
