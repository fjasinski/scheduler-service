package com.weatherdata.schedulerservice.infrastructure.service.OpenWeatherService.impl;

import com.weatherdata.schedulerservice.domain.city.CityDto;
import com.weatherdata.schedulerservice.infrastructure.service.OpenWeatherService.OpenWeatherUrlBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class OpenWeatherUrlBuilderImpl implements OpenWeatherUrlBuilder {

    @Value("${weather.service.url}")
    private String restServiceUrl;

    @Value("${weather.service.uri.weather}")
    private String restServiceWeatherUri;

    @Value("${weather.service.uri.forecast}")
    private String restServiceForecastUri;

    @Value("${weather.service.key}")
    private String restServiceKey;

    @Value("${weather.service.units}")
    private String restServiceUnits;

    @Override
    public Mono<String> presentWeatherUrl(CityDto cityDto) {
        return baseUri(cityDto).map(baseUri -> restServiceUrl + restServiceWeatherUri + baseUri);
    }

    @Override
    public Mono<String> forecastWeatherUrl(CityDto cityDto) {
        return baseUri(cityDto).map(baseUri -> restServiceUrl + restServiceForecastUri + baseUri);
    }

    /**
     * Base method which returns base URI which never changes
     * @return Mono string object
     * @param cityDto object
     */
    private Mono<String> baseUri(CityDto cityDto) {
        return Mono.fromCallable(() ->  "?units=" + restServiceUnits
                + "&q=" + cityDto.getName()
                + "," + cityDto.getCountryCode()
                + "&APPID=" + restServiceKey);
    }
}
