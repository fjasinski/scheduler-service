package com.weatherdata.schedulerservice.application;

/**
 * Data fetching service.
 */
public interface DataFetchingService {

    /**
     * Fetch data from an outside source. This could be another REST Api service.
     */
    void fetch();
}
