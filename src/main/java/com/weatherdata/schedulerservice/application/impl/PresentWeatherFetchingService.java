package com.weatherdata.schedulerservice.application.impl;

import com.weatherdata.schedulerservice.application.DataFetchingService;
import com.weatherdata.schedulerservice.domain.shared.BaseRecord;
import com.weatherdata.schedulerservice.domain.shared.RecordStoringService;
import com.weatherdata.schedulerservice.infrastructure.service.CityService.CityServiceRepository;
import com.weatherdata.schedulerservice.infrastructure.service.OpenWeatherService.OpenWeatherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PresentWeatherFetchingService implements DataFetchingService {

    @Autowired
    private CityServiceRepository cityServiceRepository;

    @Autowired
    private OpenWeatherRepository openWeatherRepository;

    @Autowired
    private RecordStoringService<BaseRecord> weatherService;

    @Override
    public void fetch() {
        cityServiceRepository
                .getCities()
                .map(city ->
                        weatherService
                                .storeRecord(openWeatherRepository.getWeatherPresent(city), city)
                                .subscribe()
                ).subscribe();
    }
}
