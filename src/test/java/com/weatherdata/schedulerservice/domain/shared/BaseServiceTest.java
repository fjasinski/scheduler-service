package com.weatherdata.schedulerservice.domain.shared;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.weatherdata.schedulerservice.domain.city.CityDto;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

public class BaseServiceTest {

    protected static String RECORD_DATE = "2019-09-12 10:01:01";
    protected static String RECORD_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private ObjectNode jsonObject;
    private CityDto cityDto;

    @Autowired
    protected DateFormatter dateFormatter;

    protected void runSetters() throws IOException {
        setCityDto();
        setJsonObject();
    }

    protected ObjectNode getJsonObject() {
        return jsonObject;
    }

    protected CityDto getCityDto() {
        return cityDto;
    }

    private void setCityDto() {
        cityDto = new CityDto();
        cityDto.setId("_id");
        cityDto.setName("London");
        cityDto.setCountryCode("UK");
    }

    private void setJsonObject() throws IOException {
         jsonObject = new ObjectMapper().readValue("{\n" +
                "  \"cod\": \"200\",\n" +
                "   \"dt\": 1568289600,\n" +
                "  \"message\": 0.0125,\n" +
                "  \"cnt\": 40,\n" +
                "  \"list\": [\n" +
                "    {\n" +
                "      \"dt\": 1568289600,\n" +
                "      \"main\": {\n" +
                "        \"temp\": 24.73,\n" +
                "        \"temp_min\": 22.64,\n" +
                "        \"temp_max\": 24.73,\n" +
                "        \"pressure\": 1027.81,\n" +
                "        \"sea_level\": 1027.81,\n" +
                "        \"grnd_level\": 1022.32,\n" +
                "        \"humidity\": 62,\n" +
                "        \"temp_kf\": 2.1\n" +
                "      },\n" +
                "      \"weather\": [\n" +
                "        {\n" +
                "          \"id\": 801,\n" +
                "          \"main\": \"Clouds\",\n" +
                "          \"description\": \"few clouds\",\n" +
                "          \"icon\": \"02d\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"clouds\": {\n" +
                "        \"all\": 20\n" +
                "      },\n" +
                "      \"wind\": {\n" +
                "        \"speed\": 5.61,\n" +
                "        \"deg\": 241.352\n" +
                "      },\n" +
                "      \"sys\": {\n" +
                "        \"pod\": \"d\"\n" +
                "      },\n" +
                "      \"dt_txt\": \"2019-09-12 12:00:00\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"city\": {\n" +
                "    \"id\": 2643743,\n" +
                "    \"name\": \"London\",\n" +
                "    \"coord\": {\n" +
                "      \"lat\": 51.5085,\n" +
                "      \"lon\": -0.1258\n" +
                "    },\n" +
                "    \"country\": \"GB\",\n" +
                "    \"population\": 1000000,\n" +
                "    \"timezone\": 3600,\n" +
                "    \"sunrise\": 1568266204,\n" +
                "    \"sunset\": 1568312646\n" +
                "  }\n" +
                "}", ObjectNode.class);
    }
}
