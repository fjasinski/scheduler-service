package com.weatherdata.schedulerservice.domain.forecast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.weatherdata.schedulerservice.domain.shared.BaseServiceTest;
import com.weatherdata.schedulerservice.domain.city.CityDto;
import com.weatherdata.schedulerservice.domain.shared.BaseRecord;
import com.weatherdata.schedulerservice.infrastructure.service.ForecastService.ForecastServiceRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ForecastServiceTest extends BaseServiceTest {

    private Method prepareData;
    private Method isRecordValid;

    @Autowired
    @InjectMocks
    private ForecastService forecastService;

    @MockBean
    private ForecastServiceRepository forecastServiceRepository;

    @Before
    public void setUp() throws IOException, NoSuchMethodException {
        runSetters();

        prepareData = ForecastService.class.getDeclaredMethod("prepareData", ObjectNode.class, CityDto.class);
        prepareData.setAccessible(true);

        isRecordValid = ForecastService.class.getDeclaredMethod("isRecordValid", Mono.class, CityDto.class);
        isRecordValid.setAccessible(true);
    }

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void prepareDataShouldReturnForecastDto() throws InvocationTargetException, IllegalAccessException {
        final Mono<ForecastDto> forecastMono = (Mono<ForecastDto>) prepareData.invoke(forecastService, getJsonObject(), getCityDto());
        forecastMono.subscribe(forecastDto -> {
            assertEquals("_id", forecastDto.getCityId());
            assertEquals(801, forecastDto.getRecordList().get(0).getWeatherId());
        });
    }

    @Test
    public void prepareDataShouldReturnEmptyForecastDtoOnObjectNodeFieldCheck() throws IOException, InvocationTargetException, IllegalAccessException {
        final ObjectNode jsonObject = new ObjectMapper().readValue("{}", ObjectNode.class);
        final CityDto cityDto = new CityDto();
        final Mono<ForecastDto> forecastMono = (Mono<ForecastDto>) prepareData.invoke(forecastService, jsonObject, cityDto);
        forecastMono.subscribe(forecastDto -> assertNull(forecastDto.getRecordList()));
    }

    @Test
    public void isRecordValidShouldReturnFalseOnLatestRecordCheck() throws InvocationTargetException, IllegalAccessException {
        final Mono<ForecastDto> forecastMono = (Mono<ForecastDto>) prepareData.invoke(forecastService, getJsonObject(), getCityDto());
        final Mono<ForecastDto> updatedForecastMono = forecastMono.flatMap(forecastDto -> {
            forecastDto.setCreatedAt(null);
            return Mono.fromCallable(() -> forecastDto);
        });

        final Mono<Boolean> boolMono = (Mono<Boolean>) isRecordValid.invoke(forecastService, updatedForecastMono, getCityDto());
        boolMono.subscribe(Assert::assertFalse);
    }

    @Test
    public void isRecordValidShouldReturnTrueOnLastSavedRecordIdCheck() throws InvocationTargetException, IllegalAccessException {
        final Mono<ForecastDto> forecastMock = Mono.fromCallable(ForecastDto::new);
        Mockito.when(forecastServiceRepository.getLatestRecordByCityId(any(CityDto.class))).thenReturn(forecastMock);
        final Mono<ForecastDto> forecastMono = (Mono<ForecastDto>) prepareData.invoke(forecastService, getJsonObject(), getCityDto());
        final Mono<Boolean> boolMono = (Mono<Boolean>) isRecordValid.invoke(forecastService, forecastMono, getCityDto());
        boolMono.subscribe(Assert::assertTrue);
    }

    @Test
    public void isRecordValidShouldReturnTrueOnLatestForecastBaseRecordCheck() throws InvocationTargetException, IllegalAccessException {
        final BaseRecord primaryBaseRecord = BaseRecord
                .builder()
                .forecastTime(dateFormatter.dateFromString(RECORD_DATE, RECORD_DATE_FORMAT))
                .build();

        final Mono<ForecastDto> forecastMock = Mono.fromCallable(() -> {
           final ForecastDto forecastDto = new ForecastDto();
           forecastDto.setId("_id");
           forecastDto.setRecordList(new ArrayList<>(Collections.singletonList(primaryBaseRecord)));
           return forecastDto;
        });

        Mockito.when(forecastServiceRepository.getLatestRecordByCityId(any(CityDto.class))).thenReturn(forecastMock);

        Mono<ForecastDto> forecastMono = (Mono<ForecastDto>) prepareData.invoke(forecastService, getJsonObject(), getCityDto());
        forecastMono = forecastMono.map(forecast -> {
            forecast.setId("_id");
            return forecast;
        });

        final Mono<Boolean> boolMono = (Mono<Boolean>) isRecordValid.invoke(forecastService, forecastMono, getCityDto());
        boolMono.subscribe(Assert::assertTrue);
    }
}
