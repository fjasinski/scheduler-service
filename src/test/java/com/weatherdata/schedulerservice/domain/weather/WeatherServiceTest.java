package com.weatherdata.schedulerservice.domain.weather;

import com.weatherdata.schedulerservice.domain.shared.*;
import com.weatherdata.schedulerservice.domain.city.CityDto;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WeatherServiceTest extends BaseServiceTest {

    private Method isRecordValid;

    @Autowired
    private BaseRecordFactory baseRecordFactory;

    @Autowired
    @InjectMocks
    private WeatherService weatherService;

    @MockBean
    private RecordRepository<BaseRecord> archiveServiceRepository;

    @Before
    public void setUp() throws NoSuchMethodException, IOException {
        runSetters();

        isRecordValid = WeatherService.class.getDeclaredMethod("isRecordValid", Mono.class, CityDto.class);
        isRecordValid.setAccessible(true);
    }

    @Test
    public void isRecordValidShouldReturnTrueOnLastRecordIdCheck() throws InvocationTargetException, IllegalAccessException {
        final Mono<BaseRecord> baseRecordMono = Mono.fromCallable(() -> BaseRecord.builder().build());
        Mockito.when(archiveServiceRepository.getLatestRecordByCityId(any(CityDto.class))).thenReturn(baseRecordMono);
        final Mono<Boolean> boolMono = (Mono<Boolean>) isRecordValid.invoke(weatherService, baseRecordMono, getCityDto());
        boolMono.subscribe(Assert::assertTrue);
    }

    @Test
    public void isRecordValidShouldReturnTrueOnLatestRecordTimestampCheck() throws InvocationTargetException, IllegalAccessException {
        final Mono<BaseRecord> baseRecordMock = Mono.fromCallable(() ->
             BaseRecord
                    .builder()
                    .cityId("_id")
                    .readingTimestamp(dateFormatter.dateFromString(RECORD_DATE, RECORD_DATE_FORMAT))
                    .build());
        Mockito.when(archiveServiceRepository.getLatestRecordByCityId(any(CityDto.class))).thenReturn(baseRecordMock);

        final Mono<BaseRecord> baseRecordMono = baseRecordFactory.getObject(getJsonObject(), getCityDto(), BaseRecordType.PRESENT);
        final Mono<Boolean> boolMono = (Mono<Boolean>) isRecordValid.invoke(weatherService, baseRecordMono, getCityDto());
        boolMono.subscribe(Assert::assertTrue);
    }
}
